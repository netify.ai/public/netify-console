# Netify Console RPM spec
# Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# RPM package details
Name: netify-console
Version: 1.2.6
Release: 1%{dist}
Summary: Netify Console
Vendor: eGloo Incorporated
License: GPLv3
Packager: eGloo Incorporated
BuildArch: noarch
Source: %{name}-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}
Requires: netifyd >= 2.82
Requires: php-cli
Requires: php-pecl-ncurses
BuildRequires: autoconf >= 2.69
BuildRequires: automake
BuildRequires: sed

%description
The Netify Console (https://www.netify.ai/) is a text-based console that
displays realtime flow information from a local or remote Netify Agent
server.

Report bugs to: https://gitlab.com/netify.ai/public/netify-console/issues

# Prepare
%prep
%setup -q

./autogen.sh
%{configure}

# Build
%build
make %{?_smp_mflags}

# Install
%install

make install DESTDIR=%{buildroot}

#rm -rf %{buildroot}/%{_bindir}

#install -d -m 0755 %{buildroot}/%{_datarootdir}/%{name}-%{version}/include

#install -D -m 0755 include/*.php %{buildroot}/%{_datarootdir}/%{name}-%{version}/include
#install -D -m 0755 nc.php %{buildroot}/%{_datarootdir}/%{name}-%{version}/nc.php

#install -D -m 0755 %{name}.sh %{buildroot}/%{_bindir}/%{name}

# Clean-up
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# Files
%files
%defattr(-,root,root)
%attr(755,root,root)%{_bindir}/%{name}
%dir %attr(755,root,root)%{_datarootdir}/%{name}-%{version}
%dir %attr(755,root,root)%{_datarootdir}/%{name}-%{version}/include
%attr(644,root,root)%{_datarootdir}/%{name}-%{version}/include/*.php
%attr(755,root,root)%{_datarootdir}/%{name}-%{version}/nc.php
%{_mandir}/man1/*

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
