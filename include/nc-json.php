<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

define('ND_JSON_VERSION', 1.9);

function nc_validate_json($json)
{
    if (! property_exists($json, 'type'))
        throw new Exception('Malformed JSON, missing type.');

    switch ($json->type) {
    case 'flow':
    case 'flow_dpi_update':
    case 'flow_dpi_complete':
        nc_validate_flow($json);
        break;

    case 'flow_stats':
        break;

    case 'flow_purge':
        break;

    case 'agent_status':
        nc_validate_agent_status($json);
        break;

    case 'agent_hello':
        nc_validate_agent_hello($json);
        break;

    case 'definitions':
        nc_validate_applications($json);
        nc_validate_protocols($json);
        break;

    default:
        throw new Exception('Unsupported JSON type: ' . $json->type);
    }

    return $json->type;
}

function nc_validate_flow($flow)
{
    if (! property_exists($flow, 'interface'))
        throw new Exception('Malformed JSON, missing interface.');
    if (! property_exists($flow, 'internal'))
        throw new Exception('Malformed JSON, missing internal.');
    if (! property_exists($flow, 'flow'))
        throw new Exception('Malformed JSON, missing flow.');
}

function nc_validate_agent_status($agent_status)
{
    if (! property_exists($agent_status, 'uptime'))
        throw new Exception('Malformed JSON, missing uptime.');
    if (! property_exists($agent_status, 'flows') &&
        ! property_exists($agent_status, 'flow_count'))
        throw new Exception('Malformed JSON, missing flow_count.');
    if (! property_exists($agent_status, 'flows_prev') &&
        ! property_exists($agent_status, 'flow_count_prev'))
        throw new Exception('Malformed JSON, missing flow_count_prev.');
    if (! property_exists($agent_status, 'maxrss_kb'))
        throw new Exception('Malformed JSON, missing maxrss_kb.');
    if (! property_exists($agent_status, 'maxrss_kb_prev'))
        throw new Exception('Malformed JSON, missing maxrss_kb_prev.');
}

function nc_validate_agent_hello($agent_hello)
{
    if (! property_exists($agent_hello, 'build_version'))
        throw new Exception('Malformed JSON, missing build_version.');
    if (! property_exists($agent_hello, 'agent_version'))
        throw new Exception('Malformed JSON, missing agent_version.');
    if (! property_exists($agent_hello, 'json_version'))
        throw new Exception('Malformed JSON, missing json_version.');
    if ($agent_hello->json_version > ND_JSON_VERSION) {
        throw new Exception('Unsupported Netify JSON version: ' .
            $agent_hello->json_version);
    }
}

function nc_validate_applications($applications)
{
    if (! property_exists($applications, 'applications'))
        throw new Exception('Malformed JSON, missing applications.');
}

function nc_validate_protocols($protocols)
{
    if (! property_exists($protocols, 'protocols'))
        throw new Exception('Malformed JSON, missing protocols.');
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
