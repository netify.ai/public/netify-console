<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function nc_socket_connect($node, $service)
{
    if ($service != 0) {
        $sd = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (!is_resource($sd))
            throw new Exception('nc_socket_connect: AF_INET(create): ' . socket_strerror(socket_last_error()));

        if (@socket_connect($sd, $node, $service) === false)
            throw new Exception('nc_socket_connect: AF_TCP: ' . socket_strerror(socket_last_error()));
    }
    else {
        $sd = socket_create(AF_UNIX, SOCK_STREAM, 0);
        if (!is_resource($sd))
            throw new Exception('nc_socket_connect: AF_UNIX(create): ' . socket_strerror(socket_last_error()));

        if (@socket_connect($sd, $node) === false)
            throw new Exception('nc_socket_connect: AF_UNIX: ' . socket_strerror(socket_last_error()));
    }

    socket_set_nonblock($sd);

    return $sd;
}

function nc_socket_select(&$fds_read = null, &$fds_write = null, &$fds_except = null)
{
    $rc = @socket_select($fds_read, $fds_write, $fds_except, 0);

    if ($rc === false)
        throw new Exception('nc_socket_select: ' . socket_strerror(socket_last_error()));

    return $rc;
}

function nc_socket_read($sd, &$json, $length = 4096, $mode = PHP_NORMAL_READ)
{
    $buffer = '';

    do {
        $chunk = @socket_read($sd, $length, $mode);

        if ($chunk === false) {
            if (socket_last_error() != 11) {
                throw new Exception('nc_socket_read: ' .
                    socket_strerror(socket_last_error()) .
                    ' [' . socket_last_error() . ']'
                );
            }

            $fds_read = array($sd);
            $fds_write = null;
            $fds_except = null;

            nc_socket_select($fds_read, $fds_write, $fds_except, 1);
            continue;
        }
        else if (strlen($chunk) == 0) return 0;

        $buffer .= $chunk;
        $length -= strlen($chunk);

    } while ($mode != PHP_NORMAL_READ && $length > 0);

    $json = json_decode($buffer);

    if ($json === null) {
        file_put_contents(sprintf('/tmp/nc-bad-json-%d.json', json_last_error()), $buffer);
        throw new Exception('JSON decode error: ' . json_last_error());
    }

    return strlen($buffer);
}

function nc_socket_read_payload($sd, &$json)
{
    $json = null;
    $length = nc_socket_read($sd, $json);

    if ($length == 0) {
        socket_close($sd);
        return 0;
    }

    if (! property_exists($json, 'length'))
        throw new Exception('Malformed JSON, missing length.');

    $payload_length = $json->length;

    $json = null;
    $length = nc_socket_read($sd, $json, $payload_length, PHP_BINARY_READ);
    if ($length == 0) return 0;

    if ($length != $payload_length)
        throw new Exception('JSON payload short read.');

    return $length;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
